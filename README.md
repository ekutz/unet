
# Pixel-Wise Gland Segmentation in histology Images

## Dataset

Gland Segmentation Challenge Contest [Competition dataset](https://warwick.ac.uk/fac/sci/dcs/research/tia/glascontest/)

## Repository

### Cell Segmentation

![Screenshot 2019-12-27 at 14.07.13](https://raw.githubusercontent.com/eikekutz/deeplearning_project/master/images/Prediction_result.png )

The network model and the training script can be seen individually in [model folder](https://github.com/eikekutz/deeplearning_project/tree/master/model)

The data augmentation and all the helper functions are located in [data_utils](https://github.com/eikekutz/deeplearning_project/tree/master/data_utils)

