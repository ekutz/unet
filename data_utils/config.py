import argparse


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def get_main_values():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, default='train', choices=['train', 'inference', 'optimize',
                                                                      'inference_test', 'train_fake_256',
                                                                      'train_comb_256'])
    parser.add_argument('--dataset', type=str, default='gland', choices=['gland', 'drive',
                                                                         'messidor', 'fake'])
    parser.add_argument('--fake_path', type=str, default=None)
    parser.add_argument('--data_aug', type=bool, default=str2bool)
    # Settings for training
    parser.add_argument('--epochs', type=int, default=100)
    parser.add_argument('--image_size', type=int, default=256)
    parser.add_argument('--dim', type=int, default=64)
    parser.add_argument('--crop_size', type=int, default=256)
    parser.add_argument('--batch_size', type=int, default=10)
    parser.add_argument('--learning_rate', type=float, default=0.0001)
    parser.add_argument('--save_epoch', type=int, default=10)
    parser.add_argument('--model_save_step', type=int, default=0)
    parser.add_argument('--train_test_ratio', type=float, default=0.75)
    # network
    parser.add_argument('--network', type=str, default='segnet', choices=['segnet', 'unet'])
    # Load pre-trained model
    parser.add_argument('--pre_trained_model_path', type=str, default=None)
    parser.add_argument('--number_of_samples', type=int, default=100)
    parser.add_argument('--base_name', type=str, default='Base11',
                        choices=['Base11', 'Base12', 'Base13', 'Base14',
                                 'Base21', 'Base22', 'Base23', 'Base24',
                                 'Base31', 'Base32', 'Base33', 'Base34'])
    # bayesian optimization
    parser.add_argument('--max_evals', type=int, default=5)

    # inference_test different datasets
    parser.add_argument('--n_random', type=int, default=100)

    # stage2 test
    parser.add_argument('--dataset_dir', type=str)
    parser.add_argument('--testmode', type=str, default='real', choices=['real', 'fake', 'comb'])

    return parser.parse_args()
