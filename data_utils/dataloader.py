from skimage.io import imread
from skimage.transform import resize
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader

from data_utils.utils import *
from data_utils.compose import *
from data_utils.dataset import CustomTensorDatasetPairedImages
import numpy as np
import torch


class CustomTensorDataset(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, img_trans=transforms.Compose([])):
        self.tensors = tensors
        # self.norm = transforms.Compose([transforms.Normalize(mean=mean,std=std)])
        self.img_trans = img_trans

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        x = self.img_trans(x)
        return x, y

    def __len__(self):
        return self.tensors[0].size(0)


def create_dataloader_messidor_inference(images, mean, std, args):
    img_trans = transforms.Compose([
        transforms.ToPILImage(),
        transforms.CenterCrop(960),
        transforms.Resize(args.crop_size),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std)
    ])

    train = CustomTensorDataset(tensors=((torch.from_numpy(np.rollaxis(images, 3, 1)),
                                          torch.from_numpy(np.stack(np.ones(len(images)))))),
                                img_trans=img_trans)
    return DataLoader(train, batch_size=args.batch_size, shuffle=False, pin_memory=True)


def create_dataloader_images(images, mean, std, options, data_aug=True):
    if data_aug:
        img_trans = transforms.Compose([
            transforms.ToPILImage(),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=mean, std=std)
        ])
    else:
        img_trans = transforms.Compose([
            transforms.Normalize(mean=mean, std=std)
        ])
    train = CustomTensorDataset(tensors=((torch.from_numpy(np.rollaxis(images, 3, 1)),
                                          torch.from_numpy(np.stack(np.ones(len(images)))))),
                                img_trans=img_trans)
    return DataLoader(train, batch_size=options.batch_size, shuffle=False, pin_memory=True)


def create_dataloader_paired_images(images, masks, args, mean, std, testset=False, data_aug=True, fake_data=False,
                                    shuffle=False):
    if args.dataset == 'messidor' and not fake_data:
        img_trans = transforms.Compose([
            transforms.ToPILImage(),
            transforms.CenterCrop(960),
            transforms.Resize(args.crop_size),
            transforms.ToTensor()])
    elif not testset and fake_data:
        if data_aug:
            img_trans = transforms.Compose([
                transforms.ToPILImage(),
                transforms.ColorJitter(brightness=0.2),
                transforms.ToTensor()
            ])
        else:
            img_trans = transforms.Compose([])
    else:
        img_trans = transforms.Compose([])
    transform = Compose([
        # ToNumpy(),
        # NumpyToTensor(),
        ToPILImage(),
        # RandomRotation(5),
        RandomHorizontalFlip(),
        RandomVerticalFlip(),
        #RandomCrop(args.crop_size),
        Resize(args.crop_size),
        ToTensor(),
    ])
    transform_no_aug = Compose([
        ToPILImage(),
        Resize(args.crop_size),
        ToTensor(),
    ])
    if not testset:
        train = CustomTensorDatasetPairedImages(
            tensors=(torch.from_numpy(np.rollaxis(images, 3, 1)),
                     torch.from_numpy(np.rollaxis(masks, 3, 1))),
            transform=transform if data_aug else transform_no_aug,
            mean=mean,
            std=std,
            img_trans=img_trans
        )
    else:
        train = CustomTensorDatasetPairedImages(
            tensors=(torch.from_numpy(np.rollaxis(images, 3, 1)),
                     torch.from_numpy(np.rollaxis(masks[..., np.newaxis], 3, 1))),
            transform=transform if data_aug else transform_no_aug,
            mean=mean,
            std=std,
            img_trans=img_trans
        )
    # return DataLoader(train, batch_size=args.batch_size, shuffle=True if testset else False, pin_memory=True)
    return DataLoader(train, batch_size=args.batch_size, shuffle=shuffle, pin_memory=True)


def create_dataloader_paired_images_test(images, masks, args, mean, std):
    transform_no_aug = Compose([
        ToPILImage(),
        Resize(args.crop_size),
        ToTensor(),
    ])
    if args.dataset == 'messidor':
        img_trans = transforms.Compose([
            transforms.ToPILImage(),
            transforms.CenterCrop(960),
            transforms.Resize(args.crop_size),
            transforms.ToTensor()])
    else:
        img_trans = transforms.Compose([])

    train = CustomTensorDatasetPairedImages(
        tensors=(torch.from_numpy(np.rollaxis(images, 3, 1)),
                 torch.from_numpy(np.rollaxis(masks[..., np.newaxis], 3, 1))),
        transform=transform_no_aug,
        mean=mean,
        std=std,
        img_trans=img_trans
    )
    return DataLoader(train, batch_size=args.batch_size, shuffle=False, pin_memory=True)
