from torchvision import transforms, utils
from torch.utils.data import Dataset
import matplotlib.pyplot as plt
import socket
DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False


def show_sample(x,y):
    plt.subplot(1, 2, 1)
    plt.imshow(x.permute(1, 2, 0))
    plt.subplot(1, 2, 2)
    plt.imshow(y[0])
    plt.show()


class CustomTensorDataset(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, transform=None, normalize=False, mean=None, std=None,
                 img_trans=transforms.Compose([])):
        if std is None:
            std = [0.5, 0.5, 0.5]
        if mean is None:
            mean = [0.5, 0.5, 0.5]
        assert all(tensors[0].size(0) == tensor.size(0) for tensor in tensors)
        self.tensors = tensors
        self.transform = transform
        self.normalize = normalize
        self.norm = transforms.Compose([transforms.Normalize(mean=mean, std=std)])
        self.img_trans = img_trans

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        if self.transform:
            # joint transformations
            x, y = self.transform(x, y)

            # individual transformations on the image
            x = self.img_trans(x)
        if self.normalize:
            x = self.norm(x)

        return x, y

    def __len__(self):
        return self.tensors[0].size(0)


class CustomTensorDatasetPairedImages(Dataset):
    """TensorDataset with support of transforms.
    """

    def __init__(self, tensors, transform=None, mean=None, std=None, img_trans=transforms.Compose([])):
        if std is None:
            std = [0.5, 0.5, 0.5]
        if mean is None:
            mean = [0.5, 0.5, 0.5]
        self.tensors = tensors
        self.transform = transform
        self.norm = transforms.Compose([transforms.Normalize(mean=mean, std=std)])
        self.img_trans = img_trans

    def __getitem__(self, index):
        x = self.tensors[0][index]
        y = self.tensors[1][index]
        x = self.img_trans(x)
        if self.transform:
            # joint transformations
            x, y = self.transform(x, y)
        x = self.norm(x)
        # if DEBUG:
        #       show_sample(x,y)
        return x, y

    def __len__(self):
        return self.tensors[0].size(0)
