from skimage.io import imread
from skimage.transform import resize
from data_utils.utils import masks_to_numpy
import os
import numpy as np
import socket

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False


def read_gland_testdata(path='gland/', image_size=64, testdata='a'):
    size_train = (image_size, image_size)
    images_test, masks_test = [], []
    if testdata == 'a':
        starter = 'testA'
    elif testdata == 'b':
        starter = 'testB'
    else:
        return None
    for root, dirs, files in os.walk(path):
        for f in files:
            try:
                if not f.endswith('anno.bmp') and not f.endswith('csv') and f.startswith(starter):
                    mask_path = f[:-4] + '_anno.bmp'
                    images_test.append(resize(imread(os.path.join(root, f)),
                                              size_train,
                                              mode='constant',
                                              anti_aliasing=True))

                    masks_test.append(
                        resize(imread(os.path.join(root, mask_path)),
                               size_train,
                               mode='constant',
                               anti_aliasing=True))
            except FileNotFoundError:
                print(f, 'could not be found')

    return masks_to_numpy(images_test), masks_to_numpy(masks_test)


def read_gland(path='gland/', image_size=64, read_images=True, read_masks=True):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    try:
        for root, dirs, files in os.walk(path):
            for f in files:
                if not f.endswith('anno.bmp') and not f.endswith('csv') and f.startswith('train'):
                    mask_path = f[:-4] + '_anno.bmp'
                    if read_images:
                        images_train.append(resize(imread(os.path.join(root, f)),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))
                    if read_masks:
                        masks_train.append(resize(imread(os.path.join(root, '..', 'anno', mask_path), plugin='pil'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True))
    except FileNotFoundError:
        print(f, 'could not be found')

    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_drive(path='retina', image_size=64, read_images=True, read_masks=True):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    for d in dirs:
        if d == 'images':
            _, _, files = next(os.walk(os.path.join(root, d)))
            files = sorted(files)
            for f in files:
                img_path = os.path.join(root, d, f)
                mask_path = os.path.join(root, '1st_manual', os.path.splitext(f)[0].split('_')[0] + '_manual1.gif')
                if os.path.exists(mask_path):
                    if read_images:
                        images_train.append(resize(imread(img_path),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))
                    if read_masks:
                        masks_train.append(resize(imread(mask_path, plugin='pil'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True))
    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_fake_data(path='messidor/', image_size=64, read_images=True, read_masks=False):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    for d in dirs:
        # if d.startswith('Base11'):
        if d.startswith('anno') and d.endswith('_anno_256'):
            path_temp = os.path.join(root, d)
            for root_2, _, files in os.walk(path_temp):
                for f in sorted(files):
                    masks_train.append(resize(imread(os.path.join(root_2, f), as_gray=True, plugin='pil', mode='L'),
                                              size_train,
                                              mode='constant',
                                              anti_aliasing=True))
        elif d.startswith('fake_256'):
            path_temp = os.path.join(root, d)
            for root_2, _, files in os.walk(path_temp):
                for f in sorted(files):
                    if not f.startswith('Annotation'):
                        images_train.append(resize(imread(os.path.join(root_2, f), plugin='pil'),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))
    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_fake_data2(path='messidor/', image_size=64, read_images=True, read_masks=False):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    for d in dirs:
        # if d.startswith('Base11'):
        if d.startswith('anno'):
            path_temp = os.path.join(root, d)
            for root_2, _, files in os.walk(path_temp):
                for f in sorted(files):
                    img_path = os.path.join(root, 'img', f)
                    mask_path = os.path.join(root_2, f)
                    if os.path.exists(img_path):
                        masks_train.append(resize(imread(mask_path, as_gray=True, plugin='pil', mode='L'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True))
                        images_train.append(resize(imread(img_path, plugin='pil'),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))

    if read_masks and not read_images:
        return masks_to_numpy(masks_train)
    if read_images and not read_masks:
        return masks_to_numpy(images_train)
    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_stage_two_test(path='', image_size=64, mode='real'):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    if mode == 'real':
        anno_dir = 'test_A'
        img_dir = 'test_B'
        img_ext = '.bmp'
    elif mode == 'test_a':
        anno_dir = 'test_C_anno'
        img_dir = 'test_C_img'
        img_ext = '.bmp'
    elif mode == 'test_b':
        anno_dir = 'test_D_anno'
        img_dir = 'test_D_img'
        img_ext = '.bmp'
    else:
        anno_dir = 'test_A'
        img_dir = 'img'
        img_ext = '.bmp'

    for d in dirs:
        # if d.startswith('Base11'):
        if d.startswith(anno_dir):
            path_temp = os.path.join(root, d)
            for root_2, _, files in os.walk(path_temp):
                for f in sorted(files):
                    img_path = os.path.join(root, img_dir, os.path.splitext(f)[0] + img_ext)
                    mask_path = os.path.join(root_2, f)
                    print(img_path,mask_path)
                    if os.path.exists(img_path):
                        masks_train.append(resize(imread(mask_path, as_gray=True, plugin='pil', mode='L'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True))
                        images_train.append(resize(imread(img_path, plugin='pil'),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))

    return masks_to_numpy(images_train), masks_to_numpy(masks_train)


def read_from_same_path(path='', image_size=64, img_dir='img', anno_dir='anno', img_ext='.bmp'):
    size_train = (image_size, image_size)
    images_train, masks_train = [], []
    root, dirs, _ = next(os.walk(path))
    print(root, dirs,img_dir)
    for d in dirs:
        # if d.startswith('Base11'):
        if d.startswith(anno_dir):
            path_temp = os.path.join(root, d)
            for root_2, _, files in os.walk(path_temp):
                for f in sorted(files):
                    img_path = os.path.join(root, img_dir, os.path.splitext(f)[0] + img_ext)
                    mask_path = os.path.join(root_2, f)
                    # print(img_path,'exists: ',os.path.exists(img_path))
                    # print(mask_path,'exists: ',os.path.exists(mask_path))
                    if os.path.exists(img_path):
                        masks_train.append(resize(imread(mask_path, as_gray=True, plugin='pil', mode='L'),
                                                  size_train,
                                                  mode='constant',
                                                  anti_aliasing=True))
                        images_train.append(resize(imread(img_path, plugin='pil'),
                                                   size_train,
                                                   mode='constant',
                                                   anti_aliasing=True))

    return masks_to_numpy(images_train), masks_to_numpy(masks_train)
