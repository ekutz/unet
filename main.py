from model.model import UNet

from model.train import training, inference_save_images, inference_test
from data_utils.config import get_main_values
from data_utils.dataloader import *
from data_utils.read_data import *
from objective import *
# Network
from torchsummary import summary
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from collections import OrderedDict
from hyperopt import fmin, tpe, Trials, hp
from data_utils.utils import ObjectView
from datetime import datetime
# from tensorboard import SummaryWriter
import socket

import matplotlib.pyplot as plt

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()):
    DATASET_DIR_GLAND = '../../Datasets/images/Warwick_QU_Dataset/'
    DATASET_DIR_RETINA = '../../Datasets/DRIVE/training'
    DATASET_DIR_MESSIDOR = '../../Datasets/Messidor/'
else:
    DATASET_DIR_GLAND = '../Datasets/Warwick_QU_Dataset/'
    DATASET_DIR_RETINA = '../Datasets/DRIVE/training'
    DATASET_DIR_MESSIDOR = '../Datasets/Messidor/'

# Set random seed for reproducibility
manualSeed = 999
# manualSeed = random.randint(1, 10000) # use if you want new results
print("Random Seed: ", manualSeed)
random.seed(manualSeed)
torch.manual_seed(manualSeed)

if __name__ == "__main__":
    # get parameters from user
    args = get_main_values()
    now = datetime.now()
    args.now = now
    # set device (eiter cpu or gpu)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)

    x_train, y_train = None, None
    x_test_a, y_test_a = None, None
    x_test_b, y_test_b = None, None
    model = None
    data_tr, data_val = None, None
    data_test_a, data_test_b = None, None
    mean, std = 0.5, 0.5
    if args.mode == 'train' or args.mode == 'optimize':
        if args.dataset == 'gland':
            x_train, y_train = read_gland(path=DATASET_DIR_GLAND, image_size=args.image_size)
            x_test_a, y_test_a = read_gland_testdata(path=DATASET_DIR_GLAND, image_size=args.crop_size,
                                                     testdata='a')
            x_test_b, y_test_b = read_gland_testdata(path=DATASET_DIR_GLAND, image_size=args.crop_size, testdata='b')
        elif args.dataset == 'retina':
            x_train, y_train = read_retina(path=DATASET_DIR_RETINA, image_size=args.image_size)
        elif args.dataset == 'messidor':
            print(DATASET_DIR_MESSIDOR)
            x_train, y_train = read_messidor(path=DATASET_DIR_MESSIDOR,
                                             image_size=args.image_size)
        elif args.dataset == 'messidor_fake':
            pass
        print(len(x_train), len(y_train))
        # Transform masks into two channel images
        print(y_train.shape)
        y_train[y_train > 0] = 1

        y_train_re = np.zeros((y_train.shape[0], y_train.shape[1], y_train.shape[2], 3), np.float32)
        for i in range(2):
            y_train_re[:, :, :, i][y_train == i] = 1

        mean = np.mean(x_train, axis=(0, 1, 2))
        std = np.std(x_train, axis=(0, 1, 2))
        # mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]

        ix = np.random.choice(len(x_train), len(x_train), False)
        tr, val = np.split(ix, [int(len(x_train) * args.train_test_ratio)])  # set better values
        data_tr = create_dataloader_paired_images(x_train[tr], y_train_re[tr], args=args, mean=mean, std=std)
        data_val = create_dataloader_paired_images(x_train[val], y_train_re[val], args=args, mean=mean, std=std,
                                                   data_aug=False)
        data_test_a, data_test_b = None, None
        # load test datasets if available
        if args.dataset == 'gland':
            data_test_a = create_dataloader_paired_images(x_test_a, y_test_a, args=args, mean=mean, std=std,
                                                          testset=True, data_aug=False)
            data_test_b = create_dataloader_paired_images(x_test_b, y_test_b, args=args, mean=mean, std=std,
                                                          testset=True, data_aug=False)

    model = UNet().to(device)
    if args.mode == 'train':
        summary(model, (3, args.crop_size, args.crop_size))

    optimizer = optim.AdamW(model.parameters(), lr=args.learning_rate)
    if args.mode == 'train':
        writer = SummaryWriter()
        if args.dataset == 'gland':
            training(model=model,
                     opt=optimizer,
                     data_tr=data_tr,
                     data_val=data_val,
                     data_test_a=data_test_a,
                     data_test_b=data_test_b,
                     device=device,
                     mean=mean,
                     std=std,
                     args=args,
                     writer=writer, )
        elif args.dataset == 'retina' or args.dataset == 'messidor':
            training(model=model,
                     opt=optimizer,
                     data_tr=data_tr,
                     data_val=data_val,
                     device=device,
                     mean=mean,
                     std=std,
                     args=args,
                     writer=writer)
    elif args.mode == 'optimize':
        SPACE = OrderedDict([
            ('model', model),
            ('optimizer', optimizer),
            ('batch_size', hp.randint('batch_size', 35) + 5),
            ('learning_rate', hp.loguniform('learning_rate', np.log(0.00001), np.log(0.0002))),
            ('device', device),
            ('mean', mean),
            ('std', std),
            ('data_tr', data_tr),
            ('data_val', data_val),
            ('options', args)

        ])
        trials = Trials()
        best = fmin(objective, SPACE, algo=tpe.suggest, max_evals=args.max_evals, trials=trials)
        with SummaryWriter(log_dir='runs_bayopt/' + now.strftime("%b%d_%H-%M-%S") + '/') as w:
            for i in range(len(trials.results)):
                hyperparams = {}
                for key in trials.vals.keys():
                    if isinstance(trials.vals[key][i], np.int64):
                        hyperparams[key] = int(trials.vals[key][i])
                    elif isinstance(trials.vals[key][i], np.float64):
                        hyperparams[key] = float(trials.vals[key][i])
                    else:
                        hyperparams[key] = trials.vals[key][i]
                w.add_hparams(hyperparams,
                              {'hparam/F1': trials.results[i]['loss']})
        print(best)

    elif args.mode == 'inference_test':

        data_test = None
        images, masks = None, None
        if args.dataset == 'messidor':
            images, masks = read_messidor(DATASET_DIR_MESSIDOR, image_size=args.image_size,
                                          read_masks=True, read_images=True, pick_random=100)
        elif args.dataset == 'messidor_fake':
            images, masks = read_fake_data(DATASET_DIR_MESSIDOR, image_size=args.image_size,
                                           read_masks=True, read_images=True)

        masks[masks > 0] = 1

        # masks_re = np.zeros((masks.shape[0], masks.shape[1], masks.shape[2], 3), np.float32)
        # for i in range(2):
        #    masks_re[:, :, :, i][masks == i] = 1

        # pick n random
        if args.dataset != 'messidor':
            ix = np.random.choice(len(images), args.n_random, False)
            images = images[ix]
            masks = masks[ix]
        print("Amount of selected images/masks: "+str(len(images))+"/"+str(len(masks)))
        mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]

        # create dataloader from it
        dataloader = create_dataloader_paired_images_test(images, masks, args=args, mean=mean, std=std)

        # load pretrained model
        if device.type == 'cpu':
            checkpoint = torch.load(args.pre_trained_model_path, map_location=lambda storage, location: storage)
        else:
            checkpoint = torch.load(args.pre_trained_model_path)
        model.load_state_dict(checkpoint['model_state_dict'])
        # start inference test
        inference_test(model, dataloader=dataloader, device=device, args=args)

    elif args.mode == 'inference':
        filenames = None
        # inference
        # load dataset
        data_inf = None
        if args.dataset == 'messidor':
            data_inf, filenames = read_messidor_with_filenames(DATASET_DIR_MESSIDOR, base_name=args.base_name)
        # mean = np.mean(data_inf, axis=(0, 1, 2))
        # std = np.std(data_inf, axis=(0, 1, 2))
        mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]
        # create dataloader
        data_loader_inf = create_dataloader_messidor_inference(data_inf, mean, std, args)
        # load model
        if device.type == 'cpu':
            checkpoint = torch.load(args.pre_trained_model_path, map_location=lambda storage, location: storage)
        else:
            checkpoint = torch.load(args.pre_trained_model_path)
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        epoch = checkpoint['epoch']
        # loss_train = checkpoint['loss_train']
        # loss_test = checkpoint['loss_test']
        f1_train = checkpoint['f1_train']
        f1_test = checkpoint['f1_test']
        print('Starting inference with F1_train: {} and F1_val: {}'.format(f1_train, f1_test))
        # model, opt, no_samples, data_loader, mean, std, options, device):
        inference_save_images(model=model,
                              opt=optimizer,
                              no_samples=args.number_of_samples,
                              dataloader=data_loader_inf,
                              mean=mean,
                              std=std,
                              options=args,
                              device=device,
                              filenames=filenames,
                              path=DATASET_DIR_MESSIDOR
                              )
        # run inference
