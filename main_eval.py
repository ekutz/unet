from model.model import UNet

from model.train import training
from data_utils.config import get_main_values
from data_utils.dataloader import *
from data_utils.read_data import *
from objective import *
# Network
from torchsummary import summary
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime
import socket
import pickle
import numpy as np
import matplotlib.pyplot as plt

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATASET_DIR_GLAND = '/Users/eike/Datasets/Warwick_QU_Dataset/train'
    DATASET_DIR_DRIVE = '/Users/eike/Datasets/DRIVE_HD/'
else:
    DATASET_DIR_GLAND = '../Datasets/Warwick_QU_Dataset/'
    DATASET_DIR_DRIVE = '../Datasets/DRIVE_HD/'

# Set random seed for reproducibility
manualSeed = 999
# manualSeed = random.randint(1, 10000) # use if you want new results
print("Random Seed: ", manualSeed)
random.seed(manualSeed)
torch.manual_seed(manualSeed)
np.random.seed(manualSeed)

if __name__ == "__main__":
    # get parameters from user
    args = get_main_values()
    now = datetime.now()
    args.now = now
    # set device (eiter cpu or gpu)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)

    x_train, y_train = None, None
    x_test_a, y_test_a = None, None
    x_test_b, y_test_b = None, None
    model = None
    data_tr, data_val = None, None
    data_test_a, data_test_b = None, None
    mean, std = 0.5, 0.5

    if args.dataset == 'drive':
        split_no = 15
        th = 0.1
    elif args.dataset == 'gland':
        split_no = 30
        th = 0
    if args.testmode == 'real':
        if args.dataset == 'gland':
            x_train, y_train = read_gland(path=DATASET_DIR_GLAND, image_size=args.image_size, read_images=True,
                                          read_masks=True)
        elif args.dataset == 'drive':
            x_train, y_train = read_from_same_path(path=DATASET_DIR_DRIVE,
                                                   image_size=args.image_size,
                                                   img_dir='train_B', anno_dir='train_A', img_ext='.jpeg')
    elif args.testmode == 'fake':
        x_train, y_train = read_from_same_path(path=args.dataset_dir,
                                               image_size=args.image_size,
                                               img_dir='img', anno_dir='test_A',
                                               img_ext='.bmp' if args.dataset == 'gland' else '.jpeg')

    elif args.testmode == 'comb':
        x_train_fake, y_train_fake = read_from_same_path(path=args.dataset_dir,
                                                         image_size=args.image_size,
                                                         img_dir='img', anno_dir='test_A',
                                                         img_ext='.bmp' if args.dataset == 'gland' else '.jpeg'
                                                         )

        if args.dataset == 'gland':
            x_train_real, y_train_real = read_gland(path=DATASET_DIR_GLAND, image_size=args.image_size,
                                                    read_images=True, read_masks=True)
        elif args.dataset == 'drive':
            x_train_real, y_train_real = read_from_same_path(path=DATASET_DIR_DRIVE,
                                                             image_size=args.image_size,
                                                             img_dir='train_B', anno_dir='train_A', img_ext='.jpeg')

        x_train = np.concatenate((x_train_fake, x_train_real), axis=0)
        y_train = np.concatenate((y_train_fake, y_train_real), axis=0)

    if args.dataset == 'gland':
        ix = np.random.choice(len(x_train), len(x_train), False)
        print(len(x_train), len(y_train))
        tr, val = np.split(ix, [int(len(x_train) * 0.75)])  # set better values
        x_val, y_val = x_train[val], y_train[val]
        x_train, y_train = x_train[tr], y_train[tr]
        x_test_a, y_test_a = read_gland_testdata(path=DATASET_DIR_GLAND,
                                                 image_size=args.image_size,
                                                 testdata='a')
        x_test_b, y_test_b = read_gland_testdata(path=DATASET_DIR_GLAND,
                                                 image_size=args.image_size,
                                                 testdata='b')
    elif args.dataset == 'drive':
        x_val, y_val = read_from_same_path(path=DATASET_DIR_DRIVE,
                                           image_size=args.image_size,
                                           img_dir='test_B', anno_dir='test_A', img_ext='.jpeg')
    # y_train[y_train > 0] = 1

    print(len(x_train), len(y_train))

    # Transform masks into two channel images
    print(x_train.shape)
    y_train[y_train > th] = 1
    y_train[y_train < th] = 0
    y_val[y_val > th] = 1
    y_val[y_val < th] = 0

    y_train_re = np.zeros((y_train.shape[0], y_train.shape[1], y_train.shape[2], 2), np.float32)
    y_val_re = np.zeros((y_val.shape[0], y_val.shape[1], y_val.shape[2], 2), np.float32)
    y_train_re[:, :, :, 1][y_train == 1] = 1
    y_train_re[:, :, :, 0][y_train == 0] = 1
    y_val_re[:, :, :, 1][y_val == 1] = 1
    y_val_re[:, :, :, 0][y_val == 0] = 1

    if args.dataset == 'gland':
        y_test_a[y_test_a > th] = 1
        y_test_a[y_test_a < th] = 0
        y_test_b[y_test_b > th] = 1
        y_test_b[y_test_b < th] = 0
        y_test_a_re = np.zeros((y_test_a.shape[0], y_test_a.shape[1], y_test_a.shape[2], 2), np.float32)
        y_test_b_re = np.zeros((y_test_b.shape[0], y_test_b.shape[1], y_test_b.shape[2], 2), np.float32)
        y_test_a_re[:, :, :, 1][y_test_a == 1] = 1
        y_test_a_re[:, :, :, 0][y_test_a == 0] = 1
        y_test_b_re[:, :, :, 1][y_test_b == 1] = 1
        y_test_b_re[:, :, :, 0][y_test_b == 0] = 1
    # Transform masks into two channel arrays (fore and background)

    mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]
    # mean = np.mean(x_train, axis=(0, 1, 2))
    # std = np.std(x_train, axis=(0, 1, 2))
    # if args.dataset=='gland':
    #     ix = np.random.choice(len(x_train), len(x_train), False)
    #     tr, val = np.split(ix, [int(len(x_train) * args.train_test_ratio)])  # set better values
    data_tr = create_dataloader_paired_images(x_train, y_train_re, args=args, mean=mean, std=std,
                                              data_aug=True if args.data_aug else False,
                                              fake_data=False,
                                              shuffle=True)
    data_val = create_dataloader_paired_images(x_val, y_val_re, args=args, mean=mean, std=std,
                                               data_aug=False,
                                               fake_data=False)

    # load test set
    if args.dataset == 'gland':
        data_test_a = create_dataloader_paired_images(x_test_a, y_test_a_re, args=args, mean=mean, std=std,
                                                      data_aug=False)
        data_test_b = create_dataloader_paired_images(x_test_b, y_test_b_re, args=args, mean=mean, std=std,
                                                      data_aug=False)

    model = UNet(dim=args.dim).to(device)
    if args.mode == 'train':
        summary(model, (3, args.crop_size, args.crop_size))

    optimizer = optim.AdamW(model.parameters(), lr=args.learning_rate)
    writer = SummaryWriter(flush_secs=1,
                           log_dir='runs_eval/' + datetime.now().strftime("%b%d_%H-%M-%S") + '/')

    training(model=model,
             opt=optimizer,
             data_tr=data_tr,
             data_val=data_val,
             data_test_a=data_test_a if args.dataset == 'gland' else None,
             data_test_b=data_test_b if args.dataset == 'gland' else None,
             device=device,
             mean=mean,
             std=std,
             args=args,
             writer=writer)
