from model.model import UNet

from model.train import training, inference_save_images, inference_test
from data_utils.config import get_main_values
from data_utils.dataloader import *
from data_utils.read_data import *
from objective import *
# Network
from torchsummary import summary
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from collections import OrderedDict
from hyperopt import fmin, tpe, Trials, hp
from data_utils.utils import ObjectView
from datetime import datetime
# from tensorboard import SummaryWriter
import socket
import pickle
import matplotlib.pyplot as plt
import cv2
import operator

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

if DEBUG:
    DATASET_DIR_MESSIDOR = '../../Datasets/Messidor/'
    DATASET_DIR_MESSIDOR_MOD = '../../Datasets/Messidor/'

else:
    DATASET_DIR_MESSIDOR = '../Datasets/Messidor/'
    DATASET_DIR_MESSIDOR_MOD = '../Datasets/Messidor/'

# Set random seed for reproducibility
manualSeed = 999
# manualSeed = random.randint(1, 10000) # use if you want new results
print("Random Seed: ", manualSeed)
random.seed(manualSeed)
torch.manual_seed(manualSeed)


def cropND(img, bounding):
    start = tuple(map(lambda a, da: a // 2 - da // 2, img.shape, bounding))
    end = tuple(map(operator.add, start, bounding))
    slices = tuple(map(slice, start, end))
    return img[slices]


def preMask(mask, mb=3, th=0.5):
    mask = np.array([cv2.medianBlur(im, mb) for im in mask])
    mask[mask > th] = 1
    mask[mask <= th] = 0
    kernel = np.ones((3, 3), np.uint8)
    mask = np.array(cv2.dilate(mask, kernel, iterations=1))
    #
    return mask[:, :, np.newaxis]
    # return mask
    # y_train[y_train > 0] = 1


if __name__ == "__main__":
    # get parameters from user
    args = get_main_values()
    now = datetime.now()
    args.now = now
    # set device (eiter cpu or gpu)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)

    x_train, y_train = None, None
    x_test_a, y_test_a = None, None
    x_test_b, y_test_b = None, None
    model = None
    data_tr, data_val = None, None
    data_test_a, data_test_b = None, None
    mean, std = 0.5, 0.5

    if DEBUG:
        ix = np.random.choice(200, 200, False)
    else:
        PIK = "random_list.dat"
        with open(PIK, "rb") as f:
            ix = np.array(pickle.load(f))

    if args.mode == 'train':
        print(DATASET_DIR_MESSIDOR)
        x_train, y_train = read_messidor2(path=DATASET_DIR_MESSIDOR,
                                          image_size=args.image_size, ix=ix[:200],
                                          read_images=True,
                                          read_masks=True,
                                          )
        x_test_a = x_train[100:150]
        y_test_a = y_train[100:150]
        x_test_b = x_train[150:200]
        y_test_b = y_train[150:200]
        x_train = x_train[:100]
        y_train = y_train[:100]
        # y_train[y_train > 0] = 1

    elif args.mode == 'train_fake_256':
        x_train, y_train = read_fake_data(path=DATASET_DIR_MESSIDOR_MOD,
                                          image_size=args.image_size,
                                          read_images=True,
                                          read_masks=True)

        x_test_a, y_test_a = read_messidor2(path=DATASET_DIR_MESSIDOR,
                                            image_size=args.image_size,
                                            pick_random=True,
                                            read_images=True,
                                            read_masks=True,
                                            ix=ix[100:200])
        x_test_b = x_test_a[50:]
        y_test_b = y_test_a[50:]
        x_test_a = x_test_a[:50]
        y_test_a = y_test_a[:50]
        # preprocessing of the masks
        # y_train = np.array([preMask(ma, th=0.2, mb=3) for ma in y_train])
        # y_train = y_train[:, :, :, 0]

    elif args.mode == 'train_comb_256':
        x_train, y_train = read_fake_data(path=DATASET_DIR_MESSIDOR_MOD,
                                          image_size=args.image_size,
                                          read_images=True,
                                          read_masks=True)
        # y_train = np.array([preMask(ma, th=0.2, mb=3) for ma in y_train])
        # y_train = y_train[:, :, :, 0]
        x_test_a, y_test_a = read_messidor2(path=DATASET_DIR_MESSIDOR,
                                            image_size=args.image_size,
                                            pick_random=True,
                                            read_images=True,
                                            read_masks=True,
                                            ix=ix[:200])
        tmp_y = y_test_a[:100]
        tmp_y[tmp_y > 0] = 1
        # centercrop real images t0 960 and resize images
        tmp_x = np.array([cv2.resize(cropND(a, (960, 960)), dsize=(256, 256)) for a in x_test_a[:100]])

        # cv2.resize(img, dsize=(54, 140)
        x_train = np.concatenate((x_train, tmp_x), axis=0)
        y_train = np.concatenate((y_train, tmp_y), axis=0)

        x_test_b = x_test_a[100:150]
        y_test_b = y_test_a[100:150]
        x_test_a = x_test_a[150:200]
        y_test_a = y_test_a[150:200]
    print(len(x_train), len(y_train))

    # Transform masks into two channel images
    print(y_train.shape)
    y_test_a[y_test_a > 0] = 1
    y_test_b[y_test_b > 0] = 1

    y_train_re = np.zeros((y_train.shape[0], y_train.shape[1], y_train.shape[2], 2), np.float32)
    # for i in range(2):
    #     y_train_re[:, :, :, i][y_train == i] = 1
    y_train_re[:, :, :, 1] = y_train
    y_train_re[:, :, :, 0][y_train == 0] = 1

    mean, std = [0.5, 0.5, 0.5], [0.5, 0.5, 0.5]
    # mean = np.mean(x_train, axis=(0, 1, 2))
    # std = np.std(x_train, axis=(0, 1, 2))
    ix = np.random.choice(len(x_train), len(x_train), False)
    tr, val = np.split(ix, [int(len(x_train) * args.train_test_ratio)])  # set better values
    data_tr = create_dataloader_paired_images(x_train[tr], y_train_re[tr], args=args, mean=mean, std=std,
                                              data_aug=True if args.data_aug else False,
                                              fake_data=True if args.mode == 'train_fake_256' or
                                                                args.mode == 'train_comb_256' else False)
    data_val = create_dataloader_paired_images(x_train[val], y_train_re[val], args=args, mean=mean, std=std,
                                               data_aug=False,
                                               fake_data=True if args.mode == 'train_fake_256' or
                                                                 args.mode == 'train_comb_256' else False)
    # load test set
    data_test_a = create_dataloader_paired_images(x_test_a, y_test_a, args=args, mean=mean, std=std,
                                                  testset=True, data_aug=False)
    data_test_b = create_dataloader_paired_images(x_test_b, y_test_b, args=args, mean=mean, std=std,
                                                  testset=True, data_aug=False)

    model = UNet(dim=args.dim).to(device)
    if args.mode == 'train':
        summary(model, (3, args.crop_size, args.crop_size))

    optimizer = optim.AdamW(model.parameters(), lr=args.learning_rate)
    writer = SummaryWriter(flush_secs=1,
                           log_dir='runs_eval/' + datetime.now().strftime("%b%d_%H-%M-%S") + '/')

    if args.dataset.startswith('messidor'):
        training(model=model,
                 opt=optimizer,
                 data_tr=data_tr,
                 data_val=data_val,
                 data_test_a=data_test_a,
                 data_test_b=data_test_b,
                 device=device,
                 mean=mean,
                 std=std,
                 args=args,
                 writer=writer)
