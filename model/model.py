import torch
import torch.nn as nn


class UNet(nn.Module):
    def __init__(self, dim=64):
        super().__init__()
        self.momentum = 0.01
        self.dim = dim
        self.en1 = self.encode_block(3, dim, 1, drop_rate=0.0)
        self.en2 = self.encode_block(dim, dim * 2, 1, drop_rate=0.0)
        self.en3 = self.encode_block(dim * 2, dim * 4, 1, drop_rate=0.0)
        self.en4 = self.encode_block(dim * 4, dim * 8, 1, drop_rate=0.5)

        self.boNe = self.bottleneck(dim * 8, dim * 16, 1, drop_rate=0.5)

        self.de4 = self.decode_block(dim * 16, dim * 8, 1, drop_rate=0.5)
        self.de3 = self.decode_block(dim * 8, dim * 4, 1, drop_rate=0.5)
        self.de2 = self.decode_block(dim * 4, dim * 2, 1, drop_rate=0.0)
        self.de1 = self.decode_block(dim * 2, dim, 1, last_layer=True)

        self.pool = nn.MaxPool2d(2, 2)

    def encode_block(self, input_channel, output_channel, no_layer, drop_rate=0.0):
        en_list = []
        en_list.extend([
            nn.Conv2d(input_channel, output_channel, 3, padding=1),
            nn.BatchNorm2d(output_channel, momentum=self.momentum, track_running_stats=False),
            nn.ReLU()
        ])
        for i in range(no_layer):
            en_list.extend([
                nn.Conv2d(output_channel, output_channel, 3, padding=1),
                nn.BatchNorm2d(output_channel, momentum=self.momentum, track_running_stats=False),
                nn.ReLU()
            ])
        if drop_rate > 0:
            en_list += [nn.Dropout(drop_rate)]
        return nn.Sequential(*en_list)

    def bottleneck(self, input_channel, output_channel, no_layer, drop_rate=0.0):
        b_list = []
        b_list.extend([
            nn.Conv2d(input_channel, output_channel, 3, padding=1),
            nn.BatchNorm2d(output_channel, momentum=self.momentum, track_running_stats=False),
            nn.ReLU()
        ])
        for i in range(no_layer):
            b_list.extend([
                nn.Conv2d(output_channel, output_channel, 3, padding=1),
                nn.BatchNorm2d(output_channel, momentum=self.momentum, track_running_stats=False),
                nn.ReLU()
            ])
        if drop_rate > 0:
            b_list += [nn.Dropout(drop_rate)]
        return nn.Sequential(*b_list)

    def upsample_cat(self, left_input, bottom_input):
        upsample = nn.Upsample(int(bottom_input.shape[-1] * 2))  # double bottom image dimensions
        bottom_input = upsample(bottom_input)
        return torch.cat([bottom_input, left_input], 1)

    def decode_block(self, input_channel, output_channel, no_layer, last_layer=False, drop_rate=0.0):
        de_list = []
        de_list.extend([
            nn.Conv2d(input_channel + int(input_channel * 0.5), output_channel, 3, padding=1),
            nn.BatchNorm2d(output_channel, momentum=self.momentum),
            nn.ReLU()
        ])
        for i in range(no_layer):
            de_list.extend([
                nn.Conv2d(output_channel, output_channel, 3, padding=1),
                nn.BatchNorm2d(output_channel, momentum=self.momentum, track_running_stats=False),
                nn.ReLU()
            ])
        if drop_rate > 0:
            de_list += [nn.Dropout(drop_rate)]
        if last_layer:
            de_list.append(nn.Conv2d(output_channel, 2, 3, padding=1))
        return nn.Sequential(*de_list)

    def forward(self, x):
        e1 = self.en1(x)
        x = self.pool(e1)
        e2 = self.en2(x)
        x = self.pool(e2)
        e3 = self.en3(x)
        x = self.pool(e3)
        e4 = self.en4(x)
        x = self.pool(e4)

        b = self.boNe(x)

        d4 = self.upsample_cat(e4, b)
        d4 = self.de4(d4)
        d3 = self.upsample_cat(e3, d4)
        d3 = self.de3(d3)
        d2 = self.upsample_cat(e2, d3)
        d2 = self.de2(d2)
        d1 = self.upsample_cat(e1, d2)
        d1 = self.de1(d1)

        return d1
