import os
import torch.nn as nn
import torchvision
# internal utilities
from time import time
from data_utils.evaluation import F1_score
from data_utils.data_augmentation import *
from data_utils.compose import UnNormalize
import matplotlib.pyplot as plt
from datetime import datetime
import socket
from PIL import Image
import torchvision.utils as vutils
from typing import Union, Optional, Sequence, Tuple, Text, BinaryIO

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False

date_time = datetime.now().strftime("%b%d_%H-%M-%S")
MODEL_PATH = 'models/' + date_time
if not os.path.exists("results/"):
    os.makedirs("results/")


def normalize(tensor, range: Optional[Tuple[int, int]] = None, scale_each: bool = False):
    tensor = tensor.clone()  # avoid modifying tensor in-place
    if range is not None:
        assert isinstance(range, tuple), \
            "range has to be a tuple (min, max) if specified. min and max are numbers"

    def norm_ip(img, min, max):
        img.clamp_(min=min, max=max)
        img.add_(-min).div_(max - min + 1e-5)

    def norm_range(t, range):
        if range is not None:
            norm_ip(t, range[0], range[1])
        else:
            norm_ip(t, float(t.min()), float(t.max()))

    if scale_each is True:
        for t in tensor:  # loop over mini-batch dimension
            norm_range(t, range)
    else:
        norm_range(tensor, range)
    return tensor


def save_image(image, path):
    image = image * 255
    im = Image.fromarray(image.numpy().astype('uint8'))
    im = im.convert("L")
    im.save(path, "PNG")


def inference_test(model, dataloader, device, args):
    model.eval()

    f1_score = []
    unnorm = UnNormalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])

    for x_batch_val, y_batch_val in dataloader:
        y_pred = torch.sigmoid(model(x_batch_val.to(device))).detach().cpu()
        for idx_, y_pred in enumerate(y_pred):
            f1_score_temp = F1_score(y_batch_val[idx_], torch.argmax(y_pred, dim=0))
            f1_score.append(f1_score_temp)
            if DEBUG:
                print(f1_score_temp)
                fig, axs = plt.subplots(1, 3, constrained_layout=True)
                axs[0].set_title('real_rgb')
                axs[0].imshow(np.transpose(unnorm(x_batch_val[idx_]), (1, 2, 0)))
                axs[1].set_title('real')
                axs[1].imshow(y_batch_val[idx_][0], cmap='gray')
                axs[2].set_title('pred')
                axs[2].imshow(torch.argmax(y_pred, dim=0), cmap='gray')
                # plt.savefig(str(idx_)+args.now.strftime("%b%d_%H-%M-%S")+".png")
                plt.show()
    f1_temp = np.array(f1_score)
    print("F1_score:", np.mean(f1_temp), '+-', np.std(f1_temp))

    log_file = open("results/F1_Score_" + args.dataset + "_" + args.now.strftime("%b%d_%H-%M-%S") + ".txt", "w")
    log_file.write('mean;std\n')
    log_file.write(str(np.mean(f1_temp)) + ';' + str(np.std(f1_temp)))
    log_file.close()

    log_file = open("results/F1_Score_raw_" + args.dataset + "_" + args.now.strftime("%b%d_%H-%M-%S") + ".txt", "w")
    log_file.write('F1_Score\n')
    for score in f1_score:
        log_file.write(str(score) + "\n")
    log_file.close()


def inference_save_images(model, opt, no_samples, dataloader, mean, std, options, device, filenames, path):
    model.eval()
    unnorm = UnNormalize(mean, std)
    counter = 0
    for x_batch_val, _ in dataloader:
        y_pred = torch.sigmoid(model(x_batch_val.to(device))).detach().cpu()

        for idx_, y_pred_n in enumerate(y_pred):
            # dpi = 200
            # fig = plt.figure(figsize=(options.crop_size / dpi, options.crop_size / dpi), dpi=dpi)
            # print(filenames[counter])
            anno = torch.argmax(y_pred_n, dim=0)
            name = os.path.join(path,
                                options.base_name + '_anno',
                                str(options.crop_size),
                                os.path.splitext(filenames[counter])[0] + '.png')
            if not os.path.exists(os.path.split(name)[0]):
                os.makedirs(os.path.split(name)[0])
            save_image(anno, name)
            # plt.imsave(name, anno, cmap='gray', dpi=dpi)
            # plt.close(fig)
            counter += 1


def training(model, opt, data_tr, data_val, device, mean, std, args, writer=None, data_test_a=None,
             data_test_b=None):
    print(model)
    criterion = nn.CrossEntropyLoss()
    x_val, y_val = next(iter(data_val))
    if data_test_a:
        x_test_a, y_test_a = next(iter(data_test_a))
    if data_test_b:
        x_test_b, y_test_b = next(iter(data_test_b))

    img_batch = np.zeros((6, 3, args.crop_size, args.crop_size))
    mask_batch_real = np.zeros((6, 1, args.crop_size, args.crop_size))

    unnorm = UnNormalize(mean, std)
    r = 4 if args.dataset == 'retina' else 2

    for i in range(r):
        # Real images from val
        img_batch[i, 0] = unnorm(x_val[i].clone()).numpy()[0]
        img_batch[i, 1] = unnorm(x_val[i].clone()).numpy()[1]
        img_batch[i, 2] = unnorm(x_val[i].clone()).numpy()[2]
        # real segmentation masks
        mask_batch_real[i, 0] = np.rollaxis(torch.argmax(y_val[i], dim=0).clone().numpy(), 0, 1)

        if data_test_a:
            # Real images from testa
            img_batch[i + 2, 0] = unnorm(x_test_a[i].clone()).numpy()[0]
            img_batch[i + 2, 1] = unnorm(x_test_a[i].clone()).numpy()[1]
            img_batch[i + 2, 2] = unnorm(x_test_a[i].clone()).numpy()[2]
            # real segmentation masks
            mask_batch_real[i + 2, 0] = np.rollaxis(torch.argmax(y_test_a[i],dim=0).clone().numpy(), 0, 1)

        if data_test_b:
            # Real images from testb
            img_batch[i + 4, 0] = unnorm(x_test_b[i].clone()).numpy()[0]
            img_batch[i + 4, 1] = unnorm(x_test_b[i].clone()).numpy()[1]
            img_batch[i + 4, 2] = unnorm(x_test_b[i].clone()).numpy()[2]
            # real segmentation masks
            mask_batch_real[i + 4, 0] = np.rollaxis(torch.argmax(y_test_b[i],dim=0).clone().numpy(), 0, 1)
    if writer:
        writer.add_images('images', img_batch, 0)
        writer.add_images('real_masks', mask_batch_real, 0)
    # loss_hist = {'train': [], 'test': []}

    for epoch in range(args.epochs):
        tic = time()

        avg_loss = 0
        avg_loss_test = 0
        f1_train = []
        model.train()  # train mode
        for x_batch, y_batch in data_tr:
            x_batch = x_batch.to(device)
            y_batch = y_batch.to(device)

            # forward
            y_pred = model(x_batch)
            if DEBUG:
                sample_images(x_batch.detach(), y_batch.detach(), torch.sigmoid(y_pred).detach(), 'train')
            loss = criterion(y_pred, torch.argmax(y_batch, dim=1).long())
            for idx_, y_pred in enumerate(y_pred):
                f1_train.append(F1_score(torch.argmax(y_batch[idx_].detach().cpu(), dim=0),
                                         torch.argmax(y_pred.detach().cpu(), dim=0)))

            # set parameter gradients to zero
            opt.zero_grad()
            # backward
            loss.backward()  # backward-pass
            torch.nn.utils.clip_grad_norm_(model.parameters(), 0.1)
            opt.step()  # update weights
            # calculate metrics to show the user
            avg_loss += loss.detach().cpu().numpy()
        avg_loss = avg_loss / len(data_tr)
        if writer:
            writer.add_scalar('Loss/train', avg_loss, epoch)

        toc = time()
        f1_temp = np.array(f1_train)
        if writer:
            writer.add_scalar('F1/train_mean', np.mean(f1_temp), epoch)
            writer.add_scalar('F1/train_std', np.std(f1_temp), epoch)

        # show intermediate results

        model.eval()  # testing mode
        # calulating loss on data_val

        f1_val = []
        for x_batch_val, y_batch_val in data_val:

            y_pred = torch.sigmoid(model(x_batch_val.to(device))).detach().cpu()
            if DEBUG:
                sample_images(x_batch_val, y_batch_val, y_pred, 'val')
            avg_loss_test += criterion(y_pred, torch.argmax(y_batch_val, dim=1).long()).detach().cpu().numpy()
            for idx_, y_pred_n in enumerate(y_pred):
                f1_val.append(F1_score(torch.argmax(y_batch_val[idx_], dim=0), torch.argmax(y_pred_n, dim=0)))

        avg_loss_test = avg_loss_test / len(data_val)
        if writer:
            writer.add_scalar('Loss/test', avg_loss_test, epoch)

        f1_temp = np.array(f1_val)
        if writer:
            writer.add_scalar('F1/val_mean', np.mean(f1_temp), epoch)
            writer.add_scalar('F1/val_std', np.std(f1_temp), epoch)
        # calulating F1 on testA,testB
        f1_test_a_ep_macro, f1_test_b_ep_macro = [], []  # temp f1 scores for all images on this epoch

        if data_test_a:
            for x_batch_val, y_batch_val in data_test_a:
                y_pred = torch.sigmoid(model(x_batch_val.to(device))).detach().cpu()
                for idx_, y_pred_n in enumerate(y_pred):
                    f1_test_a_ep_macro.append(F1_score(torch.argmax(y_batch_val[idx_], dim=0),
                                                       torch.argmax(y_pred_n, dim=0)))

            f1_temp_a = np.array(f1_test_a_ep_macro)
            if writer:
                writer.add_scalar('F1/test_a_mean', np.mean(f1_temp_a), epoch)
                writer.add_scalar('F1/test_a_std', np.std(f1_temp_a), epoch)

        if data_test_b:
            for x_batch_val, y_batch_val in data_test_b:
                y_pred = torch.sigmoid(model(x_batch_val.to(device))).detach().cpu()
                for idx_, y_pred_n in enumerate(y_pred):
                    f1_test_b_ep_macro.append(F1_score(torch.argmax(y_batch_val[idx_], dim=0),
                                                       torch.argmax(y_pred_n, dim=0)))
                    # f1_testB_ep_binary.append(F1_score(y_batch_val[idx_],y_pred,avg='binary'))

            f1_temp_b = np.array(f1_test_b_ep_macro)
            if writer:
                writer.add_scalar('F1/test_b_mean', np.mean(f1_temp_b), epoch)
                writer.add_scalar('F1/test_b_std', np.std(f1_temp_b), epoch)

        '''Save model'''
        if args.model_save_step > 0 and epoch % args.model_save_step == 0 and epoch > 0 and args.mode == 'train':
            if not os.path.exists(MODEL_PATH):
                os.makedirs(MODEL_PATH)
            save_path = '{}/epoch_{}.pt'.format(MODEL_PATH, epoch)

            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': opt.state_dict(),
                'loss_train': avg_loss,
                'loss_test': avg_loss_test,
                'f1_train': np.mean(np.array(f1_train)),
                'f1_test': np.mean(np.array(f1_val))
            }, save_path)
        '''
        Save plot after each n_th epoch
        '''
        if epoch % args.save_epoch == 0 or epoch == 0 or epoch == args.epochs:

            y_hat = model(x_val.to(device)).detach().cpu()
            if data_test_a:
                y_hat_a = model(x_test_a.to(device)).detach().cpu()
            if data_test_b:
                y_hat_b = model(x_test_b.to(device)).detach().cpu()

            # print('Y_hat.shape',Y_hat.shape)

            mask_batch_seg = np.zeros((6, 1, args.crop_size, args.crop_size))
            for i in range(2):
                mask_batch_seg[i, 0] = torch.argmax(y_hat[i], dim=0)
                if data_test_a:
                    mask_batch_seg[i + 2, 0] = torch.argmax(y_hat_a[i], dim=0)
                if data_test_b:
                    mask_batch_seg[i + 4, 0] = torch.argmax(y_hat_b[i], dim=0)
            if writer:
                writer.add_images('segmented_mask', mask_batch_seg, epoch)
            if args.mode == 'train':
                print('* Epoch %d/%d' % (epoch + 1, args.epochs),
                      'f1_train', np.mean(np.array(f1_train)),
                      'f1_val', np.mean(np.array(f1_val)))
    # return value for bayesian optimization
    if data_test_a and data_test_b:
        return np.mean(np.array([np.mean(np.array(f1_val)), np.mean(np.array(f1_test_a_ep_macro)),
                                 np.mean(np.array(f1_test_b_ep_macro))]))
    elif data_test_a and not data_test_b:
        return np.mean(np.array([np.mean(np.array(f1_val)), np.mean(np.array(f1_test_a_ep_macro))]))
    elif data_test_b and not data_test_a:
        return np.mean(np.array([np.mean(np.array(f1_val)), np.mean(np.array(f1_test_b_ep_macro))]))
    return np.mean(np.array(f1_val))


# Helper functions
def sample_images(image_batch, masks_batch, pred_batch, name='title'):
    img = np.transpose(vutils.make_grid(normalize(image_batch), padding=2, normalize=True), (1, 2, 0))
    mask = np.transpose(
        vutils.make_grid(torch.argmax(masks_batch, dim=1, keepdim=True).float(), padding=2, normalize=True), (1, 2, 0))
    pred = np.transpose(
        vutils.make_grid(torch.argmax(pred_batch, dim=1, keepdim=True).float(), padding=2, normalize=True), (1, 2, 0))
    pred0 = np.transpose(vutils.make_grid(pred_batch[:, 0].unsqueeze(1).float(), padding=2, normalize=True), (1, 2, 0))
    pred1 = np.transpose(vutils.make_grid(pred_batch[:, 1].unsqueeze(1).float(), padding=2, normalize=True), (1, 2, 0))
    f, ax = plt.subplots(5, 1, figsize=(15, 15))
    # (ax1, ax2, ax3, ax4, ax5)
    for a in ax:
        a.axis("off")
    ax[0].set_title(name)
    ax[0].imshow(img)
    ax[1].imshow(mask)
    ax[2].imshow(pred)
    ax[3].imshow(pred0)
    ax[4].imshow(pred1)
    plt.show()


def to_numpy(images, masks):
    X = np.array(images, np.float32)
    Y = np.array(masks, np.float32)
    print('Loaded %d images' % len(X))
    print('Loaded %d masks' % len(Y))
    return X, Y


def apply_transform(image, mask, sigma):
    trans_img, trans_masks = [], []
    for idx, img in enumerate(image):
        # merge to apply elastic deformation
        im_merge = np.dstack((img, mask[idx]))
        im_merge_t = elastic_transform(im_merge, im_merge.shape[1] * 2, im_merge.shape[1] * 0.08 * sigma,
                                       im_merge.shape[1] * 0.08 * sigma)
        # split it afterwards
        trans_img.append(im_merge_t[..., :-1])
        trans_masks.append(im_merge_t[..., -1] > 0)
    return trans_img, trans_masks
