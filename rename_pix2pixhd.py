import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir', type=str, default='/Users/eike/Datasets/')
    parser.add_argument('--outdir', type=str, default='/Users/eike/Datasets/')
    parser.add_argument('--ext', type=str, default='.jpeg')
    args = parser.parse_args()

    root, _, files = next(os.walk(args.dir))
    for f in files:
        print(f,f.endswith("_synthesized_image"+args.ext))
        if f.endswith("_synthesized_image"+args.ext):
            img_path = os.path.join(root, f)
            mask_path = os.path.join(root, f.split('_')[0] + "_input_label"+args.ext)
            new_img_path = os.path.join(args.outdir, 'img', f.split('_')[0] +args.ext)
            new_mask_path = os.path.join(args.outdir, 'anno', f.split('_')[0] +args.ext)
            print(img_path,new_img_path,mask_path,new_mask_path)
            os.makedirs(os.path.split(new_img_path)[0], exist_ok=True)
            os.makedirs(os.path.split(new_mask_path)[0], exist_ok=True)
            os.rename(img_path, new_img_path)
            os.rename(mask_path, new_mask_path)
    # for f in files:
    #     if f.endswith("_synthesized_image.jpeg"):
    #         img_path = os.path.join(root, f)
    #         mask_path = os.path.join(root, f.split('_')[0] + "_input_label.jpeg")
    #         new_img_path = os.path.join(args.outdir, 'img', f.split('_')[0] + '.jpeg')
    #         new_mask_path = os.path.join(args.outdir, 'anno', f.split('_')[0] + '.jpeg')
    #
    #         os.makedirs(os.path.split(new_img_path)[0], exist_ok=True)
    #         os.makedirs(os.path.split(new_mask_path)[0], exist_ok=True)
    #         os.rename(img_path, new_img_path)
    #         os.rename(mask_path, new_mask_path)
