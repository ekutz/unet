#!/bin/sh
### General options
### -- specify queue --
#BSUB -q hpc
### -- set the job Name --
#BSUB -J rename
### -- ask for number of cores (default: 1) --
#BSUB -n 1
### -- specify that the cores must be on the same host --
#BSUB -R "span[hosts=1]"
### -- specify that we need 2GB of memory per core/slot --
#BSUB -R "rusage[mem=2GB]"
### -- specify that we want the job to get killed if it exceeds 3 GB per core/slot --
#BSUB -M 10GB
### -- set walltime limit: hh:mm --
#BSUB -W 24:00
### -- set the email address --
# please uncomment the following line and put in your e-mail address,
# if you want to receive e-mail notifications on a non-default address
##BSUB -u s182902@student.dtu.dk
### -- send notification at start --
#BSUB -B
### -- send notification at completion --
#BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o Output_%J.out
#BSUB -e Error_%J.err

module load python3/3.7.5

#python3 rename_pix2pixhd.py --dir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/real/gland_sep_600_512_bin/test_latest/images --outdir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/real --ext=.bmp
python3 rename_pix2pixhd.py --dir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/ind_cell_removal/gland_sep_600_512_bin/test_latest/images/ --outdir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/ind_cell_removal/ --ext=.bmp
python3 rename_pix2pixhd.py --dir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/multiple_cell_removal/gland_sep_600_512_bin/test_latest/images/ --outdir /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/binary/multiple_cell_removal/ --ext=.bmp

#rm -rf /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/ind_cell_removal/gland/
#rm -rf /zhome/95/c/135723/Datasets/Warwick_QU_Dataset_HD/multiple_cell_removal/gland/